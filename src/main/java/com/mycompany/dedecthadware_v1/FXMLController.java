/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dedecthadware_v1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mycompany.dedecthadware_v1.rest.Alerts;
import com.mycompany.dedecthadware_v1.rest.RestCallback;
import com.mycompany.dedecthadware_v1.rest.model.ComputerInformation;
import com.squareup.okhttp.Response;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Shape;
import javafx.stage.WindowEvent;

/**
 *
 * @author largo
 */
public class FXMLController implements Initializable, Runnable {

    @FXML
    Label label_progress, label1, label2, label3;
    @FXML
    Shape circle1, circle2, circle3;
    @FXML
    Button button;
    @FXML
    TextField tfNick;
    @FXML
    PasswordField pfPassword;

    private Image imageOK  = new Image(getClass().getResourceAsStream("/images/OK.png"));
    private Image imageNeutral= new Image(getClass().getResourceAsStream("/images/NEUTRAL.png"));

    private String username = "", CPU = "", OS = "", DedicVendorID = "", DedicDeviceID = "", VendorID = "", DeviceID = "", RAM = "";

    private Alerts alert;

    public volatile int flag = 0;

    @FXML
    private void handleButtonAction(ActionEvent event) {

        Platform.runLater(() -> {
            circle1.setFill(new ImagePattern(imageNeutral));
            label_progress.setText("Autoryzacja");
            button.setDisable(true);
        });

        RestController rest = new RestController(tfNick.getText(), pfPassword.getText());

        if (tfNick.getText().equals("") || pfPassword.getText().equals("")) {
            Platform.runLater(() -> {
                alert = new Alerts();
                alert.showAlert("Wprowadź login i hasło", "Błąd autoryzacji urzytkownika");
            });
        }
        else {
            rest.getRequest(value -> {
                if (value.equals("1")) {
                    Platform.runLater(() -> {
                        circle1.setFill(new ImagePattern(imageOK));
                        label_progress.setText("Autoryzacja pomyślna");
                        username = tfNick.getText();
                    });
                    run();

                }
                else {
                    button.setDisable(false);

                    Platform.runLater(() -> {
                        alert = new Alerts();
                        alert.showAlert("Dane nie poprawne !", "Błąd autoryzacji urzytkownika");
                    });
                }
            });
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        circle1.setFill(new ImagePattern(imageNeutral));
        circle2.setFill(new ImagePattern(imageNeutral));
        circle3.setFill(new ImagePattern(imageNeutral));

        label1.setText("Check user");
        label2.setText("Analzye \n and \n send Data");
        label3.setText("Finish");

    }

    @Override
    public void run() {
        String filePath = "./dxdiag.txt";

        Thread thread2 = new Thread(() -> {
            flag = 2;
            try {

                BufferedReader br = new BufferedReader(new FileReader(filePath));
                String line;

                while ((line = br.readLine()) != null) {

                    if (line.trim().startsWith("Operating System: ")) {

                        Platform.runLater(() -> {
                            label_progress.setText("Zapisywanie danych o systemie");
                        });
                        OS = line;

                    } else if (line.trim().startsWith("Memory: ")) {

                        Platform.runLater(() -> {
                            label_progress.setText("Zapisywanie danych o pamięci");
                        });
                        RAM = line.replaceAll("[a-zA-Z: ]", "");

                    } else if (line.trim().startsWith("Processor: ")) {

                        Platform.runLater(() -> {
                            label_progress.setText("Zapisywanie danych o CPU");
                        });
                        CPU = line;

                    } else if (line.trim().startsWith("Device ID: PCI\\VEN_10DE") || line.trim().startsWith("Device ID: PCI\\VEN_1002")) {    //dedykowana jeśli jest integra

                        Platform.runLater(() -> {
                            label_progress.setText("Zapisywanie danych o GPU");
                        });
                        DedicVendorID = line.substring(19, 23);
                        DedicDeviceID = line.substring(28, 32);

                    } else if (line.trim().startsWith("Vendor ID: 0x")) {                 //dedykowana lub integra jeśli jest
                        VendorID = line.substring(23);
                    } else if (line.trim().startsWith("Device ID: 0x")) {                 //dedykowana lub integra jeśli jest
                        DeviceID = line.substring(23);
                    }

                }

                Platform.runLater(() -> {
                    RestController rest = new RestController(username, CPU,OS,RAM,VendorID,DeviceID,DedicVendorID,DedicDeviceID);
                    rest.sendRequest();
                    circle3.setFill(new ImagePattern(imageOK));
                    label_progress.setText("Zakończono");
                    alert = new Alerts();
                    alert.showAlert("Ukończono badanie konfiguracji komputera. " +
                            "Do twojego konta zostały dodane informacji o konfiguracji twojego komputera.", "Zakończono");
                    button.setDisable(false);
                });



            } catch (IOException ex) {
                Platform.runLater(() -> {
                    alert = new Alerts();
                    alert.showAlert("Problem z pobraniem danych !", "Błąd IO");
                });
            }
        });

        Thread thread = new Thread(() -> {
            flag = 1;
            try {
                circle2.setFill(new ImagePattern(imageOK));

                Platform.runLater(() -> {
                    label_progress.setText("Zbieranie informacji o komputerze");
                });

                // Use "dxdiag /t" variant to redirect output to a given file
                ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/c", "dxdiag", "/t", filePath);
                System.out.println("-- Executing dxdiag command --");
                Process p = pb.start();
                p.waitFor();

                Platform.runLater(() -> {
                    thread2.setDaemon(true);
                    thread2.start();
                });

            } catch (IOException ex) {
                Platform.runLater(() -> {
                    alert = new Alerts();
                    alert.showAlert("Problem z pobraniem danych !", "Błąd IO");
                });
            } catch (InterruptedException ex) {
                Platform.runLater(() -> {
                    alert = new Alerts();
                    alert.showAlert("Czy chcesz przerwać program ?", "Przerwanie");
                });
            }

        });

        thread.setDaemon(true);
        thread.start();
    }


}
