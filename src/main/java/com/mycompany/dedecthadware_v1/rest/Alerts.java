package com.mycompany.dedecthadware_v1.rest;

import com.mycompany.dedecthadware_v1.FXMLController;
import javafx.scene.control.Alert;

/**
 * Created by largo on 18.10.2016.
 */
public class Alerts extends FXMLController{

    private Alert alert;

    public Alerts() {
        alert = new Alert(Alert.AlertType.WARNING);
    }

    public void showAlert(String text, String title) {
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(text);
        alert.showAndWait();
    }
}
