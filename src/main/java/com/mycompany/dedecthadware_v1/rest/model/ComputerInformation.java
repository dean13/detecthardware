package com.mycompany.dedecthadware_v1.rest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ComputerInformation implements Serializable{
    private int id;

    private String username;

    private String cpu;
    private String os;
    private String ram;
    private String vendor_id;
    private String device_id;
    private String dedicate_vendor_id;
    private String dedicate_device_id;

    public ComputerInformation() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(String vendor_id) {
        this.vendor_id = vendor_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDedic_vendor_id() {
        return dedicate_vendor_id;
    }

    public void setDedic_vendor_id(String dedic_vendor_id) {
        this.dedicate_vendor_id = dedic_vendor_id;
    }

    public String getDedic_device_id() {
        return dedicate_device_id;
    }

    public void setDedic_device_id(String dedic_device_id) {
        this.dedicate_device_id = dedic_device_id;
    }

    public String getUsername() {return username;}

    public void setUsername(String username) {this.username = username;}
}
