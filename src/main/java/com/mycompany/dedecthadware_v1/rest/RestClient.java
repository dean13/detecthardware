package com.mycompany.dedecthadware_v1.rest;


import retrofit.GsonConverterFactory;
import retrofit.Retrofit;


/**
 * Created by Pantera on 2016-03-23.
 */
public class RestClient {
    private static ApiRest apiRest;

    // initialize Retrofit
    static {
        setupRestClient();
    }

    public static ApiRest get(){
        return apiRest;
    }

    private static void setupRestClient(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://gearforgame.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiRest = retrofit.create(ApiRest.class);
    }
}
