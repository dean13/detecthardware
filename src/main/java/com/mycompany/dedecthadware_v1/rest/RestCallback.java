package com.mycompany.dedecthadware_v1.rest;

/**
 * Created by largo on 09.10.2016.
 */
public interface RestCallback {
    void onSuccess(String value);
}
