/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dedecthadware_v1.rest;

import com.google.gson.JsonElement;
import com.mycompany.dedecthadware_v1.rest.model.ComputerInformation;
import com.squareup.okhttp.ResponseBody;
import retrofit.Call;

import retrofit.http.*;

/**
 *
 * @author largo
 */
public interface ApiRest {

    @POST("/rest/check/{username}/{password}")
    Call<ResponseBody> getUser(@Path("username") String username, @Path("password") String password);


    @POST("/rest/api")
    Call<ComputerInformation> sendData(@Body ComputerInformation computerInformation);
}
