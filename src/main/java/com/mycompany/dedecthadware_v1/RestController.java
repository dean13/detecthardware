/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dedecthadware_v1;

import com.mycompany.dedecthadware_v1.rest.RestCallback;
import com.mycompany.dedecthadware_v1.rest.RestClient;
import com.mycompany.dedecthadware_v1.rest.model.ComputerInformation;
import com.squareup.okhttp.ResponseBody;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import java.io.IOException;

/**
 *
 * @author largo
 */
public class RestController extends FXMLController{
    
    private String username, password,cpu, os, vendor_id, device_id, dedicate_vendor_id, dedicate_device_id, ram;

    public RestController(String username, String cpu ,String os, String ram, String vendor_id, String device_id, String dedicate_vendor_id, String dedicate_device_id) {
        this.username = username;
        this.cpu = cpu;
        this.os = os;
        this.ram = ram;
        this.vendor_id = vendor_id;
        this.device_id = device_id;
        this.dedicate_vendor_id = dedicate_vendor_id;
        this.dedicate_device_id = dedicate_device_id;
    }

    public RestController(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
    public void sendRequest() {
        ComputerInformation computerInformation = new ComputerInformation();
        computerInformation.setOs(this.os);
        computerInformation.setVendor_id(this.vendor_id);
        computerInformation.setDevice_id(this.device_id);
        computerInformation.setCpu(this.cpu);
        computerInformation.setRam(this.ram);
        computerInformation.setDedic_vendor_id(this.dedicate_vendor_id);
        computerInformation.setDedic_device_id(this.dedicate_device_id);
        computerInformation.setUsername(this.username);

        Call<ComputerInformation> response = RestClient.get().sendData(computerInformation);
        response.enqueue(new Callback<ComputerInformation>() {
            @Override
            public void onResponse(Response<ComputerInformation> response, Retrofit retrofit) {
                System.out.print(response.body()+" "+retrofit.baseUrl());
            }

            @Override
            public void onFailure(Throwable throwable) {
                System.out.println("Callback eroor"+throwable);
            }
        });
    }

    public void getRequest(final RestCallback callback) {
        Call<ResponseBody> myCall = RestClient.get().getUser(username, password);
        myCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                // handle success
                try {
                    callback.onSuccess(response.body().string());
                }catch (IOException e) {

                }
            }

            @Override
            public void onFailure(Throwable t) {
                // handle failure
            }
        });
    }
}
